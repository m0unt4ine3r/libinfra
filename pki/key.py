'''
Copyright 2021 m0unt4ine3r

This file is part of libinfra.

libinfra is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libinfra is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libinfra.  If not, see <https://www.gnu.org/licenses/>.
'''


from __future__ import annotations

from typing import Union, Callable, Optional
from abc import ABCMeta, abstractmethod

from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives import serialization

import attr

from datastructs import Collection


# TODO: add transparency for "BestAvailableEncryption"
@attr.s(str=False)
class Base(metaclass=ABCMeta):
    _interface = attr.ib()
    encoding: serialization.Encoding = attr.ib(
        default=serialization.Encoding.PEM
    )
    format = attr.ib(
        default=None
    )
    _key: bytes = attr.ib(default=None, init=False)

    def __str__(self):
        key = self._key.decode()
        return key

    @classmethod
    @abstractmethod
    def from_pem(cls, pem: bytes):
        ...

    @property
    def interface(self):
        return self._interface


@attr.s(str=False, repr=False)
class Private(Base):
    encryption_algorithm: Union[
        Optional[Callable[[bytes], serialization.KeySerializationEncryption]],
    ] = attr.ib(default=serialization.BestAvailableEncryption)
    _locked: bool = attr.ib(default=False, init=False)

    def __attrs_post_init__(self):
        self.format: serialization.PrivateFormat =\
            serialization.PrivateFormat.PKCS8
        self._key = self.interface.private_bytes(
            self.encoding, self.format, serialization.NoEncryption()
        )

    def __repr__(self):
        key_bytes = self.interface.public_bytes(
            serialization.Encoding.Raw,
            serialization.PublicFormat.Raw,
            serialization.NoEncryption()
        )
        repr_str = '<PrivateKey('\
            + f'is_locked={self.is_locked}, '\
            + f'encoding={self.encoding}, '\
            + f'format={self.format}, '\
            + f'encryption_algortihm={self.encryption_algorithm}, '\
            + f'key={key_bytes}, '\
            + ')>'
        return repr_str

    @property
    def is_locked(self):
        return self._locked

    @classmethod
    def from_pem(cls, pem, password=None):
        interface = serialization.load_pem_private_key(pem, password)
        key = cls(interface)
        return key

    def lock(self, password: bytes):
        if self.is_locked:
            raise Exception('this key is already locked')

        encryption_algorithm = self.encryption_algorithm
        if encryption_algorithm is None:
            raise Exception('no encryption algorithm set')

        self._key = self.interface.private_bytes(
            self.encoding,
            self.format,
            self.encryption_algorithm(password)             # type: ignore
        )
        # TODO: should also clean the key from memory here
        self._interface = None
        self._locked = True

    def unlock(self, password: bytes):
        if not self.is_locked:
            raise Exception('this key is not locked')

        self._interface = serialization.load_pem_private_key(
            self._key, password
        )
        self._key = self.interface.private_bytes(
            self.encoding, self.format, serialization.NoEncryption()
        )

    def sign(self, data: bytes):
        return self.interface.sign(data)


@attr.s(str=False, repr=False)
class Public(Base):
    def __attrs_post_init__(self):
        self.format: serialization.PublicFormat =\
            serialization.PublicFormat.SubjectPublicKeyInfo
        self._key = self.interface.public_bytes(
            self.encoding, self.format
        )

    def __repr__(self):
        key_bytes = self.interface.public_bytes(
            serialization.Encoding.Raw, serialization.PublicFormat.Raw
        )
        repr_str = '<PublicKey('\
            + f'encoding={self.encoding}, '\
            + f'format={self.format}, '\
            + f'key_bytes={key_bytes}'\
            + ')>'
        return repr_str

    @classmethod
    def from_pem(cls, pem):
        interface = serialization.load_pem_public_key(pem)
        key = cls(interface)
        return key

    def verify(self, signature, data):
        return self.interface.verify(signature, data)


@attr.s(repr=False)
class Pair:
    _private_key: Private = attr.ib(default=None)
    _public_key: Public = attr.ib(default=None)
    _algorithm = attr.ib(default=None)
    # TODO: make algorithm enum or something else

    def __repr__(self):
        repr_str = '<KeyPair('\
            + f'keypair_algorithm={self._algorithm}, '\
            + f'public_key={self.public!r}'\
            + ')>'
        return repr_str

    @classmethod
    def generate(
        cls,
        algorithm=Ed25519PrivateKey
    ) -> Pair:
        private_key_interface = algorithm.generate()
        private_key = Private(private_key_interface)

        public_key_interface = private_key.interface.public_key()
        public_key = Public(public_key_interface)

        key_pair = cls(
            private_key,
            public_key,
        )
        return key_pair

    @property
    def private(self) -> Private:
        return self._private_key

    @property
    def public(self) -> Public:
        return self._public_key


class Ring(Collection):
    def __repr__(self):
        other = self.copy()
        repr_str = f'<KeyRing({other!r})>'
        return repr_str

    def __setitem__(self, key: str, value: Pair):
        if not isinstance(value, Pair):
            raise TypeError(
                'value must be a key.Pair or subclass of key.Pair'
            )

        dict.__setitem__(self, str(key), value)


# -------
