'''
Copyright 2021 m0unt4ine3r

This file is part of libinfra.

libinfra is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libinfra is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libinfra.  If not, see <https://www.gnu.org/licenses/>.
'''


from __future__ import annotations

import datetime

from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

import attr

import key
import certificate
import helpers


DEFAULT_CERTIFICATE_LIFE = 256  # days


@attr.s(repr=False)
class Entity:
    # must be able to handle:
    common_name: str = attr.ib()
    country_name: str = attr.ib(default=None)
    organization_name: str = attr.ib(default=None)
    organizational_unit_name: str = attr.ib(default=None)
    dn_qualifier: str = attr.ib(default=None)
    state_or_province_name: str = attr.ib(default=None)
    serial_number: str = attr.ib(default=None)

    # should be able to handle:
    locality_name: str = attr.ib(default=None)
    title: str = attr.ib(default=None)
    surname: str = attr.ib(default=None)
    given_name: str = attr.ib(default=None)
    #initials: str = attr.ib(default=None)
    pseudonym: str = attr.ib(default=None)
    generation_qualifier: str = attr.ib(default=None)
    domain_component: str = attr.ib(default=None)

    _repr_start: str = attr.ib(default='<Entity(', init=False)

    def __str__(self):
        return str(self.common_name)

    def __repr__(self):
        repr_str = self._repr_start
        for oid_attr in self.oid_attributes:
            attr_value = getattr(self, oid_attr)
            if attr_value is not None:
                repr_str = repr_str + f'{oid_attr}={attr_value!r}, '

        repr_str = repr_str[:-2] + ')>'
        return repr_str

    @property
    def oid_attributes(self) -> list[str]:
        oid_attrs: list[str] = []
        for name_oid_attr in dir(NameOID):
            if '__' not in name_oid_attr:
                print(name_oid_attr)
                oid_attr = name_oid_attr.lower()
                oid_tuple = (self, oid_attr)
                if hasattr(*oid_tuple):
                    oid_attrs.append(oid_attr)

        return oid_attrs

    @property
    def x509_name(self) -> x509.Name:
        name_attributes: list[x509.NameAttribute] = []
        for oid_attribute in self.oid_attributes:
            oid_value = getattr(self, oid_attribute)
            if oid_value is not None:
                nameoid_attribute = oid_attribute.upper()
                oid = getattr(NameOID, nameoid_attribute)
                name_attributes.append(x509.NameAttribute(oid, oid_value))

        x509_name = x509.Name(name_attributes)
        return x509_name


@attr.s(repr=False)
class Principal(Entity):
    keys: key.Ring = attr.ib(default=key.Ring())
    requests: certificate.Binder = attr.ib(default=certificate.Binder())
    certificates: certificate.Binder = attr.ib(default=certificate.Binder())

    _repr_start: str = attr.ib(default='<Principal(', init=False)

    def make_certificate_request(
        self,
        signing_keypair: key.Pair,
        extensions: list[tuple[x509.ExtensionType, bool]] = None,
        csr_attributes: list[tuple[x509.ObjectIdentifier, bytes]] = None,
        hash_algorithm: hashes.HashAlgorithm = None
    ) -> certificate.Requested:
        x509_name = self.x509_name
        builder = x509.CertificateSigningRequestBuilder()
        builder = builder.subject_name(x509_name)

        if extensions is not None:
            for extension in extensions:
                builder = builder.add_extension(*extension)

        if csr_attributes is not None:
            for attribute in csr_attributes:
                builder = builder.add_attribute(*attribute)

        csr = builder.sign(
            signing_keypair.private.interface,
            hash_algorithm                      # type: ignore
        )
        wrapped_csr = certificate.Requested(
            interface=csr, public_key=signing_keypair.public
        )
        return wrapped_csr

    def sign_certificate_request(
        self,
        certificate_request: certificate.Requested,
        signing_keypair: key.Pair,
        expires_on: datetime.datetime = helpers.days_from_now(
            DEFAULT_CERTIFICATE_LIFE
        ),
        extensions: list[tuple[x509.ExtensionType, bool]] = None
    ) -> certificate.Signed:
        builder = x509.CertificateBuilder()
        builder = builder.subject_name(certificate_request.subject)
        builder = builder.public_key(certificate_request.public_key.interface)
        builder = builder.issuer_name(self.x509_name)
        builder = builder.serial_number(x509.random_serial_number())
        builder = builder.not_valid_before(
            datetime.datetime.now(datetime.timezone.utc)
        )
        builder = builder.not_valid_after(expires_on)

        if extensions is not None:
            for extension in extensions:
                builder = builder.add_extension(*extension)

        cert = builder.sign(
            signing_keypair.private.interface,
            certificate_request.signature_hash_algorithm
        )
        signed_cert = certificate.Signed(interface=cert)
        return signed_cert


@attr.s
class CertificateAuthority(Principal):
    '''CA'''

    def verify_certificate_request(
        self,
        certificate_request: certificate.Requested
    ) -> bool:
        '''verify the request conforms to specific requirement'''


@attr.s
class RootCA(CertificateAuthority):
    ...


@attr.s
class IntermediateCA(CertificateAuthority):
    ...


@attr.s
class SigningCA(IntermediateCA):
    ...



# -------
