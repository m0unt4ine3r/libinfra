'''
Copyright 2021 m0unt4ine3r

This file is part of libinfra.

libinfra is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libinfra is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libinfra.  If not, see <https://www.gnu.org/licenses/>.
'''


from __future__ import annotations

from abc import ABCMeta

import subprocess

from cryptography import x509
from cryptography.hazmat.primitives import serialization

import attr

import key
from datastructs import Collection


DEFAULT_CERTIFICATE_LIFE = 256  # days


@attr.s
class Base(metaclass=ABCMeta):
    _interface = attr.ib(default=None)
    _public_key: key.Public = attr.ib(default=None)
    encoding: serialization.Encoding = attr.ib(
        default=serialization.Encoding.PEM
    )

    @property
    def certificate(self):
        return self._interface.public_bytes(self.encoding)

    @property
    def subject(self):
        return self._interface.subject

    @property
    def public_key(self):
        return self._public_key

    @property
    def extensions(self):
        return self._interface.extensions

    @property
    def signature_algorithm(self):
        '''https://www.rfc-editor.org/rfc/rfc5280#section-4.1.2.3'''
        return self._interface.signature_algorithm_oid

    @property
    def signature(self):
        return self._interface.signature

    @property
    def signature_hash_algorithm(self):
        return self._interface.signature_hash_algorithm


@attr.s(str=False, repr=False)
class Requested(Base):
    def __attrs_post_init__(self):
        self._interface: x509.CertificateSigningRequest

    def __str__(self):
        cmd_openssl_x509 = 'openssl req -text -noout'.split()
        with subprocess.Popen(
            cmd_openssl_x509, stdin=subprocess.PIPE, stdout=subprocess.PIPE
        ) as openssl_x509:
            str_str = openssl_x509.communicate(self.certificate)[0].decode()

        return str_str

    def __repr__(self):
        repr_str = '<RequestedCertificate('\
            + f'subject={self.subject}, '\
            + f'public_key={self.public_key!r}, '\
            + f'extensions={self.extensions}, '\
            + f'signature_algorithm={self.signature_algorithm}, '\
            + f'signature={self.signature}'\
            + ')>'
        return repr_str


@attr.s(str=False, repr=False)
class Signed(Base):
    def __attrs_post_init__(self):
        self._interface: x509.Certificate

    def __str__(self):
        cmd_openssl_x509 = 'openssl x509 -text -noout'.split()
        with subprocess.Popen(
            cmd_openssl_x509, stdin=subprocess.PIPE, stdout=subprocess.PIPE
        ) as openssl_x509:
            str_str = openssl_x509.communicate(self.certificate)[0].decode()

        return str_str

    def __repr__(self):
        repr_str = '<SignedCertificate('\
            + f'version={self.version}, '\
            + f'serial_number={self.serial_number}, '\
            + f'issuer={self.issuer}, '\
            + f'not_valid_before={self.not_valid_before}, '\
            + f'not_valid_after={self.not_valid_after}, '\
            + f'subject={self.subject}, '\
            + f'public_key={self.public_key}, '\
            + f'extensions={self.extensions}, '\
            + f'signature_algorithm={self.signature_algorithm}, '\
            + f'signature={self.signature}'\
            + ')>'
        return repr_str

    @property
    def version(self):
        '''https://www.rfc-editor.org/rfc/rfc5280#section-4.1.2.1'''

        version = self._interface.version
        version_str = f'{version.name}({version.value})'
        return version_str

    @property
    def serial_number(self):
        '''https://www.rfc-editor.org/rfc/rfc5280#section-4.1.2.2'''

        return self._interface.serial_number

    @property
    def issuer(self):
        return self._interface.issuer

    @property
    def not_valid_before(self):
        return self._interface.not_valid_before

    @property
    def not_valid_after(self):
        return self._interface.not_valid_after


@attr.s
class Binder(Collection):
    def __repr__(self):
        other = self.copy()
        repr_str = f'<Binder({other!r})>'
        return repr_str

    def __setitem__(self, key: str, value: Base):
        if not isinstance(value, Base):
            raise TypeError(
                'value must be a certificate.Base or subclass of '
                + 'certificate.Base'
            )

        dict.__setitem__(self, str(key), value)


# -------
