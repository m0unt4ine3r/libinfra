'''
Copyright 2021 m0unt4ine3r

This file is part of libinfra.

libinfra is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libinfra is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libinfra.  If not, see <https://www.gnu.org/licenses/>.
'''


from abc import ABCMeta


class Collection(dict, metaclass=ABCMeta):
    def __init__(self, **kwargs):
        self._default = 'default'
        for key, value in kwargs.items():
            self[key] = value
            if key[0:2] == '__':
                self._default = value

    def __repr__(self):
        other = self.copy()
        repr_str = f'<Container({other!r})>'
        return repr_str

    @property
    def default(self):
        default = self.default
        return {default: self[default]}

    @default.setter
    def default(self, key: str):
        try:
            self._default = self[str(key)]

        except KeyError:
            raise KeyError(
                '{key} is not a key in this Container; '
                + f'key must be in {list(self)}'
            )

    @default.deleter
    def default(self):
        del self[self.default]
        for key in self:
            self.default = key
            break


# -------
